﻿namespace DES_Algorithm
{
    partial class desForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(desForm));
            this.configurationGroupBox = new System.Windows.Forms.GroupBox();
            this.randomKeyCheckBox = new System.Windows.Forms.CheckBox();
            this.keyLabel = new System.Windows.Forms.Label();
            this.keyTextBox = new System.Windows.Forms.TextBox();
            this.inDataGroupBox = new System.Windows.Forms.GroupBox();
            this.openBitsTextBox = new System.Windows.Forms.TextBox();
            this.decypherButton = new System.Windows.Forms.Button();
            this.openTextBox = new System.Windows.Forms.TextBox();
            this.cypherButton = new System.Windows.Forms.Button();
            this.outDataGroupBox = new System.Windows.Forms.GroupBox();
            this.resultStringTextBox = new System.Windows.Forms.TextBox();
            this.resultBitsTextBox = new System.Windows.Forms.TextBox();
            this.oneRoundRadioButton = new System.Windows.Forms.RadioButton();
            this.sixteenRoundsRadioButton = new System.Windows.Forms.RadioButton();
            this.roundKeysDataGrid = new System.Windows.Forms.DataGridView();
            this.roundNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roundKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.configurationGroupBox.SuspendLayout();
            this.inDataGroupBox.SuspendLayout();
            this.outDataGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roundKeysDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // configurationGroupBox
            // 
            this.configurationGroupBox.AutoSize = true;
            this.configurationGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.configurationGroupBox.Controls.Add(this.sixteenRoundsRadioButton);
            this.configurationGroupBox.Controls.Add(this.randomKeyCheckBox);
            this.configurationGroupBox.Controls.Add(this.oneRoundRadioButton);
            this.configurationGroupBox.Controls.Add(this.keyLabel);
            this.configurationGroupBox.Controls.Add(this.keyTextBox);
            this.configurationGroupBox.Location = new System.Drawing.Point(16, 15);
            this.configurationGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.configurationGroupBox.Name = "configurationGroupBox";
            this.configurationGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.configurationGroupBox.Size = new System.Drawing.Size(533, 97);
            this.configurationGroupBox.TabIndex = 0;
            this.configurationGroupBox.TabStop = false;
            this.configurationGroupBox.Text = "Конфигурация";
            // 
            // randomKeyCheckBox
            // 
            this.randomKeyCheckBox.AutoSize = true;
            this.randomKeyCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.randomKeyCheckBox.Checked = true;
            this.randomKeyCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.randomKeyCheckBox.Location = new System.Drawing.Point(207, 53);
            this.randomKeyCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.randomKeyCheckBox.Name = "randomKeyCheckBox";
            this.randomKeyCheckBox.Size = new System.Drawing.Size(141, 21);
            this.randomKeyCheckBox.TabIndex = 3;
            this.randomKeyCheckBox.Text = "Случайный ключ";
            this.randomKeyCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.randomKeyCheckBox.UseVisualStyleBackColor = true;
            this.randomKeyCheckBox.CheckedChanged += new System.EventHandler(this.randomKeyCheckBox_CheckedChanged);
            // 
            // keyLabel
            // 
            this.keyLabel.AutoSize = true;
            this.keyLabel.Location = new System.Drawing.Point(8, 27);
            this.keyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.keyLabel.Name = "keyLabel";
            this.keyLabel.Size = new System.Drawing.Size(47, 17);
            this.keyLabel.TabIndex = 3;
            this.keyLabel.Text = "Ключ:";
            // 
            // keyTextBox
            // 
            this.keyTextBox.Location = new System.Drawing.Point(64, 23);
            this.keyTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.keyTextBox.MaxLength = 56;
            this.keyTextBox.Name = "keyTextBox";
            this.keyTextBox.ReadOnly = true;
            this.keyTextBox.Size = new System.Drawing.Size(461, 22);
            this.keyTextBox.TabIndex = 4;
            this.keyTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // inDataGroupBox
            // 
            this.inDataGroupBox.AutoSize = true;
            this.inDataGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.inDataGroupBox.Controls.Add(this.openBitsTextBox);
            this.inDataGroupBox.Controls.Add(this.decypherButton);
            this.inDataGroupBox.Controls.Add(this.openTextBox);
            this.inDataGroupBox.Controls.Add(this.cypherButton);
            this.inDataGroupBox.Location = new System.Drawing.Point(16, 126);
            this.inDataGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.inDataGroupBox.Name = "inDataGroupBox";
            this.inDataGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.inDataGroupBox.Size = new System.Drawing.Size(535, 288);
            this.inDataGroupBox.TabIndex = 1;
            this.inDataGroupBox.TabStop = false;
            this.inDataGroupBox.Text = "Входные данные";
            // 
            // openBitsTextBox
            // 
            this.openBitsTextBox.Location = new System.Drawing.Point(8, 167);
            this.openBitsTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.openBitsTextBox.Multiline = true;
            this.openBitsTextBox.Name = "openBitsTextBox";
            this.openBitsTextBox.ReadOnly = true;
            this.openBitsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.openBitsTextBox.Size = new System.Drawing.Size(517, 98);
            this.openBitsTextBox.TabIndex = 5;
            // 
            // decypherButton
            // 
            this.decypherButton.Location = new System.Drawing.Point(271, 129);
            this.decypherButton.Margin = new System.Windows.Forms.Padding(4);
            this.decypherButton.Name = "decypherButton";
            this.decypherButton.Size = new System.Drawing.Size(256, 31);
            this.decypherButton.TabIndex = 4;
            this.decypherButton.Text = "Расшифровать";
            this.decypherButton.UseVisualStyleBackColor = true;
            this.decypherButton.Click += new System.EventHandler(this.decypherButton_Click);
            // 
            // openTextBox
            // 
            this.openTextBox.Location = new System.Drawing.Point(8, 23);
            this.openTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.openTextBox.Multiline = true;
            this.openTextBox.Name = "openTextBox";
            this.openTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.openTextBox.Size = new System.Drawing.Size(517, 98);
            this.openTextBox.TabIndex = 3;
            // 
            // cypherButton
            // 
            this.cypherButton.Location = new System.Drawing.Point(8, 129);
            this.cypherButton.Margin = new System.Windows.Forms.Padding(4);
            this.cypherButton.Name = "cypherButton";
            this.cypherButton.Size = new System.Drawing.Size(256, 31);
            this.cypherButton.TabIndex = 3;
            this.cypherButton.Text = "Зашифровать";
            this.cypherButton.UseVisualStyleBackColor = true;
            this.cypherButton.Click += new System.EventHandler(this.cypherButton_Click);
            // 
            // outDataGroupBox
            // 
            this.outDataGroupBox.AutoSize = true;
            this.outDataGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.outDataGroupBox.Controls.Add(this.resultStringTextBox);
            this.outDataGroupBox.Controls.Add(this.resultBitsTextBox);
            this.outDataGroupBox.Location = new System.Drawing.Point(16, 422);
            this.outDataGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.outDataGroupBox.Name = "outDataGroupBox";
            this.outDataGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.outDataGroupBox.Size = new System.Drawing.Size(533, 250);
            this.outDataGroupBox.TabIndex = 2;
            this.outDataGroupBox.TabStop = false;
            this.outDataGroupBox.Text = "Результат";
            // 
            // resultStringTextBox
            // 
            this.resultStringTextBox.Location = new System.Drawing.Point(8, 129);
            this.resultStringTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.resultStringTextBox.Multiline = true;
            this.resultStringTextBox.Name = "resultStringTextBox";
            this.resultStringTextBox.ReadOnly = true;
            this.resultStringTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.resultStringTextBox.Size = new System.Drawing.Size(517, 98);
            this.resultStringTextBox.TabIndex = 5;
            // 
            // resultBitsTextBox
            // 
            this.resultBitsTextBox.Location = new System.Drawing.Point(8, 23);
            this.resultBitsTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.resultBitsTextBox.Multiline = true;
            this.resultBitsTextBox.Name = "resultBitsTextBox";
            this.resultBitsTextBox.ReadOnly = true;
            this.resultBitsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.resultBitsTextBox.Size = new System.Drawing.Size(517, 98);
            this.resultBitsTextBox.TabIndex = 4;
            // 
            // oneRoundRadioButton
            // 
            this.oneRoundRadioButton.AutoSize = true;
            this.oneRoundRadioButton.Checked = true;
            this.oneRoundRadioButton.Location = new System.Drawing.Point(11, 52);
            this.oneRoundRadioButton.Name = "oneRoundRadioButton";
            this.oneRoundRadioButton.Size = new System.Drawing.Size(80, 21);
            this.oneRoundRadioButton.TabIndex = 3;
            this.oneRoundRadioButton.TabStop = true;
            this.oneRoundRadioButton.Text = "1 раунд";
            this.oneRoundRadioButton.UseVisualStyleBackColor = true;
            this.oneRoundRadioButton.CheckedChanged += new System.EventHandler(this.oneRoundRadioButton_CheckedChanged);
            // 
            // sixteenRoundsRadioButton
            // 
            this.sixteenRoundsRadioButton.AutoSize = true;
            this.sixteenRoundsRadioButton.Location = new System.Drawing.Point(97, 52);
            this.sixteenRoundsRadioButton.Name = "sixteenRoundsRadioButton";
            this.sixteenRoundsRadioButton.Size = new System.Drawing.Size(103, 21);
            this.sixteenRoundsRadioButton.TabIndex = 4;
            this.sixteenRoundsRadioButton.Text = "16 раундов";
            this.sixteenRoundsRadioButton.UseVisualStyleBackColor = true;
            this.sixteenRoundsRadioButton.CheckedChanged += new System.EventHandler(this.sixteenRoundsRadioButton_CheckedChanged);
            // 
            // roundKeysDataGrid
            // 
            this.roundKeysDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.roundKeysDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.roundNumber,
            this.roundKey});
            this.roundKeysDataGrid.Location = new System.Drawing.Point(558, 15);
            this.roundKeysDataGrid.Name = "roundKeysDataGrid";
            this.roundKeysDataGrid.RowHeadersWidth = 51;
            this.roundKeysDataGrid.RowTemplate.Height = 24;
            this.roundKeysDataGrid.Size = new System.Drawing.Size(671, 657);
            this.roundKeysDataGrid.TabIndex = 3;
            // 
            // roundNumber
            // 
            this.roundNumber.HeaderText = "Раунд";
            this.roundNumber.MinimumWidth = 6;
            this.roundNumber.Name = "roundNumber";
            this.roundNumber.ReadOnly = true;
            this.roundNumber.Width = 125;
            // 
            // roundKey
            // 
            this.roundKey.HeaderText = "Ключ раунда";
            this.roundKey.MinimumWidth = 6;
            this.roundKey.Name = "roundKey";
            this.roundKey.ReadOnly = true;
            this.roundKey.Width = 500;
            // 
            // desForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1298, 766);
            this.Controls.Add(this.roundKeysDataGrid);
            this.Controls.Add(this.outDataGroupBox);
            this.Controls.Add(this.inDataGroupBox);
            this.Controls.Add(this.configurationGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "desForm";
            this.Text = "Мифическая программа DES";
            this.configurationGroupBox.ResumeLayout(false);
            this.configurationGroupBox.PerformLayout();
            this.inDataGroupBox.ResumeLayout(false);
            this.inDataGroupBox.PerformLayout();
            this.outDataGroupBox.ResumeLayout(false);
            this.outDataGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roundKeysDataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox configurationGroupBox;
        private System.Windows.Forms.GroupBox inDataGroupBox;
        private System.Windows.Forms.GroupBox outDataGroupBox;
        private System.Windows.Forms.CheckBox randomKeyCheckBox;
        private System.Windows.Forms.Label keyLabel;
        private System.Windows.Forms.TextBox keyTextBox;
        private System.Windows.Forms.Button decypherButton;
        private System.Windows.Forms.TextBox openTextBox;
        private System.Windows.Forms.Button cypherButton;
        private System.Windows.Forms.TextBox openBitsTextBox;
        private System.Windows.Forms.TextBox resultStringTextBox;
        private System.Windows.Forms.TextBox resultBitsTextBox;
        private System.Windows.Forms.RadioButton sixteenRoundsRadioButton;
        private System.Windows.Forms.RadioButton oneRoundRadioButton;
        private System.Windows.Forms.DataGridView roundKeysDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn roundNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn roundKey;
    }
}

