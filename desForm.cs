﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DES_Algorithm
{
    public partial class desForm : Form
    {
        DES.Key key = new DES.Key();

        public desForm()
        {
            InitializeComponent();
            keyTextBox.Text = key.GetKey();
        }

        private int GetRoundNumber()
        {
            if (oneRoundRadioButton.Checked)
            {
                return 1;
            }
            else
            {
                return 16;
            }
        }

        private void cypherButton_Click(object sender, EventArgs e)
        {
            DES.Encryptor encryptor = new DES.Encryptor(openTextBox.Text, GetRoundNumber(), key);
            if(openTextBox.Text != "")
            {
                if (Regex.IsMatch(openTextBox.Text, @"[^10# ]+"))
                {
                    openTextBox.Text = encryptor.GetTextOnStart();
                    openBitsTextBox.Text = encryptor.GetBitsOnStart();
                    encryptor.Encrypt();
                    resultBitsTextBox.Text = encryptor.GetEncryptedBits();
                    resultStringTextBox.Text = encryptor.GetEncryptedText();
                }
                else
                {
                    encryptor.SetBits(Regex.Replace(openTextBox.Text, @" ", ""));
                    openBitsTextBox.Text = encryptor.GetBitsOnStart();
                    encryptor.Encrypt();
                    resultBitsTextBox.Text = encryptor.GetEncryptedBits();
                    resultStringTextBox.Text = encryptor.GetEncryptedText();
                }
                PopulateRoundKeys();
            }            
        }

        private void decypherButton_Click(object sender, EventArgs e)
        {
            DES.Decryptor decryptor = new DES.Decryptor(openTextBox.Text, GetRoundNumber(), key);
            if(openTextBox.Text != "")
            {
                if (Regex.IsMatch(openTextBox.Text, @"[^10# ]+"))
                {
                    openTextBox.Text = decryptor.GetTextOnStart();
                    openBitsTextBox.Text = decryptor.GetBitsOnStart();
                    decryptor.Decrypt();
                    resultBitsTextBox.Text = decryptor.GetDecryptedBits();
                    resultStringTextBox.Text = decryptor.GetDecryptedText();
                }
                else
                {
                    decryptor.SetBits(Regex.Replace(openTextBox.Text, @" ", ""));
                    openBitsTextBox.Text = decryptor.GetBitsOnStart();
                    decryptor.Decrypt();
                    resultBitsTextBox.Text = decryptor.GetDecryptedBits();
                    resultStringTextBox.Text = decryptor.GetDecryptedText();
                }
                PopulateRoundKeys();
            }            
        }

        private void roundNumberUpDown_ValueChanged(object sender, EventArgs e)
        {
            if(openTextBox.Text != "")
            {
                cypherButton_Click(sender, e);
            }
        }

        private void PopulateRoundKeys()
        {
            roundKeysDataGrid.Rows.Clear();
            List<string> roundKeys = key.GetRoundKeys();
            int roundNumber = 1;
            if (oneRoundRadioButton.Checked)
            {
                string[] row = { roundNumber.ToString(), roundKeys[0]};
                roundKeysDataGrid.Rows.Add(row);
            }
            else
            {
                foreach (string roundKey in roundKeys)
                {
                    string[] row = { roundNumber.ToString(), roundKey };
                    roundKeysDataGrid.Rows.Add(row);
                    roundNumber++;
                }
            }
            key.ClearRoundKeys();
        }

        private void randomKeyCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (randomKeyCheckBox.Checked)
            {
                keyTextBox.ReadOnly = true;
                key.GenerateKey();
                keyTextBox.Text = key.GetKey();
            }
            else
            {
                keyTextBox.ReadOnly = false;
                keyTextBox.Text = "";
            }
        }

        private void oneRoundRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            cypherButton_Click(sender, e);
        }

        private void sixteenRoundsRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            oneRoundRadioButton_CheckedChanged(sender, e);
        }
    }
}
