﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DES_Algorithm
{
    public class DES
    {
        private static readonly IReadOnlyDictionary<string, string> SwapTable = new Dictionary<string, string>
        {
            {"000000", "1110"}, {"000001", "0000"}, {"000010", "0100"}, {"000011", "1111"},
            {"000100", "1101"}, {"000101", "0111"}, {"000110", "0001"}, {"000111", "0100"},
            {"001000", "0010"}, {"001001", "1110"}, {"001010", "1111"}, {"001011", "0010"},
            {"001100", "1011"}, {"001101", "1101"}, {"001110", "1000"}, {"001111", "0001"},
            {"010000", "0011"}, {"010001", "1010"}, {"010010", "1010"}, {"010011", "0110"},
            {"010100", "0110"}, {"010101", "1100"}, {"010110", "1100"}, {"010111", "1011"},
            {"011000", "0101"}, {"011001", "1001"}, {"011010", "1001"}, {"011011", "0101"},
            {"011100", "0000"}, {"011101", "0011"}, {"011110", "0111"}, {"011111", "1000"},
            {"100000", "0100"}, {"100001", "1111"}, {"100010", "0001"}, {"100011", "1100"},
            {"100100", "1110"}, {"100101", "1000"}, {"100110", "1000"}, {"100111", "0010"},
            {"101000", "1101"}, {"101001", "0100"}, {"101010", "0110"}, {"101011", "1001"},
            {"101100", "0010"}, {"101101", "0001"}, {"101110", "1011"}, {"101111", "0111"},
            {"110000", "1111"}, {"110001", "0101"}, {"110010", "1100"}, {"110011", "1011"},
            {"110100", "1001"}, {"110101", "0011"}, {"110110", "0111"}, {"110111", "1110"},
            {"111000", "0011"}, {"111001", "1010"}, {"111010", "1010"}, {"111011", "0000"},
            {"111100", "0101"}, {"111101", "0110"}, {"111110", "0000"}, {"111111", "1101"}
        };

        private string XOR(string s1, string s2)
        {
            string result = "";

            for (int i = 0; i < s1.Length; i++)
            {
                bool a = Convert.ToBoolean(Convert.ToInt32(s1[i].ToString()));
                bool b = Convert.ToBoolean(Convert.ToInt32(s2[i].ToString()));

                if (a ^ b)
                    result += "1";
                else
                    result += "0";
            }
            return result;
        }

        private string Function(string s1, string key)
        {
            string block = s1;
            String[] sublocks = new String[8];
            for (int k = 0; k < 8; k++)
            {
                sublocks[k] = block.Substring(0, 4);
                block = block.Remove(0, 4);
            }

            String[] added_sublocks = new String[8];
            added_sublocks[0] = sublocks[7][3] + sublocks[0] + sublocks[1][0];
            for (int k = 1; k < 7; k++)
            {
                added_sublocks[k] = sublocks[k - 1][3] + sublocks[k] + sublocks[k + 1][0];
            }
            added_sublocks[7] = sublocks[6][3] + sublocks[7] + sublocks[0][0];

            string addedBlock = "";
            foreach (string sublock in added_sublocks)
            {
                addedBlock += sublock;
            }

            string keyBlocked =  XOR(addedBlock, key);

            string finalBlock = "";
            for (int k = 0; k < 8; k++)
            {
                finalBlock += SwapTable[keyBlocked.Substring(0, 6)];
                keyBlocked = keyBlocked.Remove(0, 6);
            }

            return finalBlock;
        }

        private class BitsConverter : DES
        {
            public string ToBits(string str)
            {
                byte[] bytes = Encoding.Unicode.GetBytes(str);
                String strBits = "";

                foreach (byte _byte in bytes)
                {
                    string strByte = Convert.ToString(_byte, 2);

                    while (strByte.Length < 8)
                    {
                        strByte = strByte.Insert(0, "0");
                    }

                    strBits += strByte;
                }

                return strBits;
            }

            public string ToStr(string str)
            {
                byte[] bytes = new byte[str.Length / 8];

                int k = 0;
                for (int i = 0; i < str.Length; i += 8)
                {
                    bytes[k] = Convert.ToByte(str.Substring(i, 8), 2);
                    k++;
                }

                return Encoding.Unicode.GetString(bytes);
            }
        }

        public class Encryptor : DES
        {
            private static readonly int SizeOfBlock = 64;
            private static readonly int SizeOfChar = 16;

            private Key key;
            private int numberOfRounds;
            private string[] blocks;

            private string textOnStart; //Нужны для отображения в интерфейсе
            private string bitsOnStart;
            private string encryptedText;
            private string encryptedBits;

            public Encryptor(string openText, int rounds, Key obj)
            {
                ResetOutputStrings();
                key = obj;
                numberOfRounds = rounds;
                SplitTextOnBlocks(openText);
            }

            private void ResetOutputStrings()
            {
                textOnStart = null;
                bitsOnStart = null;
                encryptedText = null;
                encryptedBits = null;
            }

            public string GetTextOnStart()
            {
                return textOnStart;
            }

            private string OneRound(string input, string key)
            {
                string blockL = input.Substring(0, input.Length / 2);
                string blockR = input.Substring(input.Length / 2, input.Length / 2);

                return (blockR + XOR(blockL, Function(blockR, key)));
            }

            public string GetBitsOnStart()
            {
                String startBits = bitsOnStart;
                String returnBits = "";

                for (int i = 0; i < bitsOnStart.Length; i += 8)
                {
                    returnBits += bitsOnStart.Substring(i, 8) + " ";
                }

                return returnBits;
            }

            public string GetEncryptedText()
            {
                return encryptedText;
            }

            public string GetEncryptedBits()
            {
                String startBits = encryptedBits;
                String returnBits = "";

                for (int i = 0; i < encryptedBits.Length; i += 8)
                {
                    returnBits += encryptedBits.Substring(i, 8) + " ";
                }

                return returnBits;
            }

            public void Encrypt()
            {
                for (int roundNumber = 1; roundNumber <= numberOfRounds; roundNumber++)
                {
                    key.ShiftLeft(roundNumber);

                    for (int k = 0; k < blocks.Length; k++)
                    {
                        blocks[k] = OneRound(blocks[k], key.GetRoundKey());
                    }
                }

                foreach (string block in blocks)
                {
                    encryptedBits += block;
                }

                BitsConverter bitsConverter = new BitsConverter();
                encryptedText = bitsConverter.ToStr(encryptedBits);

                key.ResetSubKey();
            }

            private void SplitTextOnBlocks(string text)
            {
                BitsConverter bitsConverter = new BitsConverter();
                string openText = ChangeTextToRightLenght(text);
                blocks = new string[(openText.Length * SizeOfChar) / SizeOfBlock];
                string openBits = bitsConverter.ToBits(openText);

                for (int i = 0; i < blocks.Length; i++)
                {
                    blocks[i] = openBits.Substring(0, SizeOfBlock);
                    bitsOnStart += blocks[i];
                    openBits = openBits.Remove(0, SizeOfBlock);
                }
            }

            private string ChangeTextToRightLenght(string text)
            {
                string newText = text;
                while ((newText.Length * SizeOfChar) % SizeOfBlock != 0)
                {
                    newText += "#";
                }
                textOnStart = newText;
                return newText;
            }

            private void SplitBitsOnBlocks(string text)
            {
                blocks = new string[text.Length / SizeOfBlock];
                string openBits = text;

                for (int i = 0; i < blocks.Length; i++)
                {
                    blocks[i] = openBits.Substring(0, SizeOfBlock);
                    bitsOnStart += blocks[i];
                    openBits = openBits.Remove(0, SizeOfBlock);
                }
            }

            public void SetBits(string bits)
            {
                blocks = null;
                bitsOnStart = "";
                SplitBitsOnBlocks(bits);
            }
        }

        public class Decryptor : DES
        {
            private static readonly int SizeOfBlock = 64;
            private static readonly int SizeOfChar = 16;

            private Key key;
            private int numberOfRounds;
            private string[] blocks;

            private string textOnStart; //Нужны для отображения в интерфейсе
            private string bitsOnStart;
            private string decryptedText;
            private string decryptedBits;

            public Decryptor(string openText, int rounds, Key obj)
            {
                ResetOutputStrings();
                key = obj;
                numberOfRounds = rounds;
                SplitTextOnBlocks(openText);
            }

            public void SetBits(string bits)
            {
                blocks = null;
                bitsOnStart = "";
                SplitBitsOnBlocks(bits);
            }

            private void ResetOutputStrings()
            {
                textOnStart = null;
                bitsOnStart = null;
                decryptedText = null;
                decryptedBits = null;
            }

            public string GetTextOnStart()
            {
                return textOnStart;
            }

            private string OneRound(string input, string key)
            {
                string blockL = input.Substring(0, input.Length / 2);
                string blockR = input.Substring(input.Length / 2, input.Length / 2);

                return (XOR(blockR, Function(blockL, key)) + blockL);
            }

            public string GetBitsOnStart()
            {
                String startBits = bitsOnStart;
                String returnBits = "";

                for (int i = 0; i < bitsOnStart.Length; i += 8)
                {
                    returnBits += bitsOnStart.Substring(i, 8) + " ";
                }

                return returnBits;
            }

            public string GetDecryptedText()
            {
                return decryptedText;
            }

            public string GetDecryptedBits()
            {
                String startBits = decryptedBits;
                String returnBits = "";

                for (int i = 0; i < decryptedBits.Length; i += 8)
                {
                    returnBits += decryptedBits.Substring(i, 8) + " ";
                }

                return returnBits;
            }

            public void Decrypt()
            {
                for (int i = 1; i <= numberOfRounds; i++)
                {
                    key.ShiftLeft(i);
                }

                for (int roundNumber = numberOfRounds; roundNumber > 0; roundNumber--)
                {
                    for (int k = 0; k < blocks.Length; k++)
                    {
                        blocks[k] = OneRound(blocks[k], key.GetRoundKey());
                    }
                    key.ShiftRight(roundNumber);
                }

                foreach (string block in blocks)
                {
                    decryptedBits += block;
                }

                BitsConverter bitsConverter = new BitsConverter();
                decryptedText = bitsConverter.ToStr(decryptedBits);

                key.ResetSubKey();
            }

            private void SplitTextOnBlocks(string text)
            {
                BitsConverter bitsConverter = new BitsConverter();
                string openText = ChangeTextToRightLenght(text);
                blocks = new string[(openText.Length * SizeOfChar) / SizeOfBlock];
                string openBits = bitsConverter.ToBits(openText);

                for (int i = 0; i < blocks.Length; i++)
                {
                    blocks[i] = openBits.Substring(0, SizeOfBlock);
                    bitsOnStart += blocks[i];
                    openBits = openBits.Remove(0, SizeOfBlock);
                }
            }

            private void SplitBitsOnBlocks(string text)
            {
                blocks = new string[text.Length / SizeOfBlock];
                string openBits = text;

                for (int i = 0; i < blocks.Length; i++)
                {
                    blocks[i] = openBits.Substring(0, SizeOfBlock);
                    bitsOnStart += blocks[i];
                    openBits = openBits.Remove(0, SizeOfBlock);
                }
            }

            private string ChangeTextToRightLenght(string text)
            {
                string newText = text;
                while ((newText.Length * SizeOfChar) % SizeOfBlock != 0)
                {
                    newText += "#";
                }
                textOnStart = newText;
                return newText;
            }
        }

        public class Key : DES
        {
            private static readonly IReadOnlyDictionary<int, int> ShiftTable = new Dictionary<int, int>
            {
                {01, 1}, {02, 1}, {03, 2}, {04, 2},
                {05, 2}, {06, 2}, {07, 2}, {08, 2},
                {09, 1}, {10, 2}, {11, 2}, {12, 2},
                {13, 2}, {14, 2}, {15, 2}, {16, 1}
            };
            private static readonly int[] PBox = new int[]
            {
                14, 17, 11, 24, 01, 05, 03, 28,
                15, 06, 21, 10, 23, 19, 12, 04,
                26, 08, 16, 07, 27, 20, 13, 02,
                41, 52, 31, 37, 47, 55, 30, 40,
                51, 45, 33, 48, 44, 49, 39, 56,
                34, 53, 46, 42, 50, 36, 29, 32
            };
            private static readonly int KeyLenght = 56;
            private string key;
            private string subKey;
            private List<string> roundKeys;

            public Key()
            {
                GenerateKey();
                roundKeys = new List<string>();
            }

            public void GenerateKey()
            {
                Random rng = new Random();
                String newKey = "";
                for (int i = 0; i < KeyLenght; i++)
                {
                    newKey += rng.Next(2).ToString();
                }
                key = newKey.ToString();
                subKey = key;
            }

            public void ResetSubKey()
            {
                subKey = key;
            }

            public string GetKey()
            {
                return key;
            }

            public string GetRoundKey()
            {
                string roundKey = "";
                foreach (int i in PBox)
                {
                    roundKey += subKey[i - 1];
                }
                if (roundKeys.Count < 16)
                {
                    roundKeys.Add(roundKey);
                }
                return roundKey;
            }

            public void SetKey(string newkey)
            {
                key = newkey;
                CheckKey();
            }

            public void ShiftLeft(int roundNumber)
            {
                int shifts = ShiftTable[roundNumber];
                string blockC = subKey.Substring(0, 28);
                string blockD = subKey.Substring(28, 28);
                for (int i = 0; i < shifts; i++)
                {
                    blockC = blockC + blockC[0];
                    blockC = blockC.Remove(0, 1);
                    blockD = blockD + blockD[0];
                    blockD = blockD.Remove(0, 1);
                }
                subKey = blockC + blockD;
            }

            public void ShiftRight(int roundNumber)
            {
                int shifts = ShiftTable[roundNumber];
                string blockC = subKey.Substring(0, 28);
                string blockD = subKey.Substring(28, 28);
                for (int i = 0; i < shifts; i++)
                {
                    blockC = blockC[blockC.Length - 1] + blockC;
                    blockC = blockC.Remove(blockC.Length - 1);
                    blockD = blockD[blockD.Length - 1] + blockD;
                    blockD = blockD.Remove(blockD.Length - 1);
                }
                subKey = blockC + blockD;
            }

            private void CheckKey()
            {
                if (key.Length < KeyLenght)
                {
                    GenerateKey();
                }
            }

            public List<string> GetRoundKeys()
            {
                return roundKeys;
            }

            public void ClearRoundKeys()
            {
                roundKeys.Clear();
            }
        }
    }
}
